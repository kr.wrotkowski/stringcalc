package com.katas;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StringCalculatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void add_with_empty_argumet_returns_zero() {
        assertEquals(0, new StringCalculator().add(""));
    }

    @Test
    public void add_with_multiple_blanks_returns_zero() {
        assertEquals(0, new StringCalculator().add("     "));
    }

    @Test
    public void add_with_one_number_returns_it() {
        assertEquals(1, new StringCalculator().add("1"));
    }

    @Test
    public void add_with_one_longer_number_returns_it() {
        assertEquals(123, new StringCalculator().add("123"));
    }

    @Test
    public void add_with_two_numbers_adds_them() {
        assertEquals(6, new StringCalculator().add("3,3"));
    }

    @Test
    public void add_with_non_numeric_should_throw() {
        expectedException.expect(NumberFormatException.class);
        expectedException.expectMessage("For input string: \"xD\"");
        new StringCalculator().add("xD");
    }

    @Test
    public void add_with_three_number_sums_them() {
        assertEquals(10, new StringCalculator().add("3,3,4"));
    }

    @Test
    public void add_with_one_or_more_numbers_sums_them() {
        assertEquals(3, new StringCalculator().add("3"));
        assertEquals(6, new StringCalculator().add("3,3"));
        assertEquals(9, new StringCalculator().add("3,3,3"));
        assertEquals(12, new StringCalculator().add("3,3,3,3"));
    }

    @Test
    public void add_with_comma_and_number_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add("3,");
    }

    @Test
    public void add_with_number_and_comma_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add(",3");
    }

    @Test
    public void add_with_comma_number_comma_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add(",3,");
    }

    @Test
    public void add_with_number_newline_numbers_sums() {
        assertEquals(6, new StringCalculator().add("3\n3"));
    }

    @Test
    public void add_with_newline_and_comma_numbers_sums() {
        assertEquals(9, new StringCalculator().add("3\n3,3"));
    }

    @Test
    public void add_with_number_newline_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add("1\n");
    }

    @Test
    public void add_with_number_comma_newline_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add("1,\n");
    }

    @Test
    public void add_with_comma_number_comma_newline_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add(",1,\n");
    }

    @Test
    public void add_with_newline_comma_number_comma_newline_throws() {
        expectedException.expect(IllegalArgumentException.class);
        new StringCalculator().add("\n,1,\n");
    }

    @Test
    public void add_with_custom_delimiter_sums() {
        assertEquals(3, new StringCalculator().add("//xD\n1xD2"));
    }

    @Test
    public void add_with_semicolon_delimiter_sums() {
        assertEquals(666, new StringCalculator().add("//;\n333;333"));
    }

    @Test
    public void add_with_star_delimiter_sums() {
        assertEquals(666, new StringCalculator().add("//*\n333*333"));
    }

    @Test
    public void add_with_negative_argument_throws() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Negatives not allowed: [-10]");
        new StringCalculator().add("-10");
    }

    @Test
    public void add_with_two_negative_argument_throws_and_prints_them() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Negatives not allowed: [-10, -2]");
        new StringCalculator().add("-10\n-2");
    }

    @Test
    public void add_with_two_negative_argument_custom_delimite_throws_and_prints_negatives() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Negatives not allowed: [-10, -2]");
        new StringCalculator().add("//xD\n-10xD-2");
    }

    @Test
    public void add_with_negative_argument_custom_delimite_throws_and_prints_negative() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Negatives not allowed: [-10]");
        new StringCalculator().add("//xD\n-10xD2");
    }

    @Test
    public void add_with_1000_sums() {
        assertEquals(1333, new StringCalculator().add("//;\n1000;333"));
    }

    @Test
    public void add_with_number_bigger_than_1000_are_ignored() {
        assertEquals(333, new StringCalculator().add("//;\n1001;333"));
    }

    @Test
    public void add_with_two_larger_than_1000_returns_0() {
        assertEquals(0, new StringCalculator().add("//;\n1001;2137"));
    }

    @Test
    public void add_with_delimiter_with_30_elements_works() {
        assertEquals(
                8,
                new StringCalculator().add("//WS0C6ae9Bzp2PvZgV6RsUJa14x7Bov\n1WS0C6ae9Bzp2PvZgV6RsUJa14x7Bov7"));
    }

    @Test
    public void add_with_delimiter_with_special_elements_works() {
        assertEquals(8, new StringCalculator().add("//*&^#@%^\n1*&^#@%^7"));
    }

    @Test
    public void add_with_two_delimiters_sum() {
        assertEquals(6, new StringCalculator().add("//[^][&]\n2&2^2"));
    }

    @Test
    public void add_with_thre_delimiters_sum() {
        assertEquals(8, new StringCalculator().add("//[_][@][(]\n2_2@2(2"));
    }

    @Test
    public void add_two_delimiter_words() {
        assertEquals(6, new StringCalculator().add("//[one][two]\n2one2two2"));
    }

    @Test
    public void add_two_delimiter_words_and_meta_chars() {
        final var calculationsArgument = "//[one][two][&][*][{}][[]\n2one2two2&2*2{}2[2";
        assertEquals(14, new StringCalculator().add(calculationsArgument));
    }
}
