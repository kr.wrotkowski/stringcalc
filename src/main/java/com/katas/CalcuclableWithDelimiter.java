package com.katas;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalcuclableWithDelimiter {

    private static Pattern delimiterGroupPattern =
            Pattern.compile("^/{2}(?<delimiter>.*)\\n(?<calculables>.*)");

    private static Pattern delimiterDynamicPattern =
            Pattern.compile("(?<=\\[)([^]]+)(?=])");

    private final String delimiter;
    private final String calculables;

    public CalcuclableWithDelimiter(final String calcuclabeWithDelimiter) {
        final var calculablesMatcher = delimiterGroupPattern.matcher(calcuclabeWithDelimiter);
        if (calculablesMatcher.find()) {
            this.delimiter = getCalculatedDelimiter(calculablesMatcher.group("delimiter"));
            this.calculables = calculablesMatcher.group("calculables");
        } else {
            this.delimiter = ",";
            calculables = calcuclabeWithDelimiter;
        }
    }

    private String getCalculatedDelimiter(final String calculablesMatcher) {
        final var matcher = delimiterDynamicPattern.matcher(calculablesMatcher);
        if (matcher.find()) {
            return calculateDelimiter(matcher);
        }
        return Pattern.quote(calculablesMatcher);
    }

    private String calculateDelimiter(final Matcher matcher) {
        final var delimiterBuilder = new StringBuilder();
        delimiterBuilder.append(Pattern.quote(matcher.group(0)));

        while (matcher.find()) {
            if (!matcher.hitEnd()) {
                delimiterBuilder.append("|");
            }
            delimiterBuilder.append(Pattern.quote(matcher.group(0)));
        }
        return delimiterBuilder.toString();
    }

    public CalcuclableWithDelimiter(final String delimiter, final String calculables) {
        this.delimiter = delimiter;
        this.calculables = calculables;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public String getCalculables() {
        return calculables;
    }
}
