package com.katas;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringCalculator {

    private Pattern correctArgumentPattern = Pattern.compile("^[\\n,].*|.*[\\n,]+$");

    public int add(final String calculationsArgument) {
        if (correctArgumentPattern.matcher(calculationsArgument).find()) {
            throw new IllegalArgumentException();
        }
        final var calculableWithDelimiter = new CalcuclableWithDelimiter(calculationsArgument);
        final var numbersList = calculableWithDelimiter.getCalculables().lines()
                .map(it -> it.split(calculableWithDelimiter.getDelimiter()))
                .flatMap(Stream::of)
                .filter(it -> !it.isBlank())
                .map(Integer::parseInt)
                .filter(it -> it <= 1000)
                .collect(Collectors.toList());

        final Supplier<Stream<Integer>> streamSupplier = numbersList::stream;

        final var negativesStream = streamSupplier.get().filter(it -> it < 0).toArray();
        if (negativesStream.length > 0) {
            throw new IllegalArgumentException("Negatives not allowed: " + Arrays.toString(negativesStream));
        }

        return streamSupplier.get()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
