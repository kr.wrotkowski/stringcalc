group = "com.katas"
description = "String Cata Calculator"
plugins {
    java
}

java {
    targetCompatibility = JavaVersion.VERSION_11
    sourceCompatibility = JavaVersion.VERSION_11
}

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    testImplementation("junit:junit:4.12")
}
