package com.katas;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CalcuclableWithDelimiterTest {

    @Test
    public void gets_delimiter_initialized_with() {
        final var calcuclableWithDelimiter = new CalcuclableWithDelimiter(",", "2,2");
        assertEquals(",", calcuclableWithDelimiter.getDelimiter());
        assertEquals("2,2", calcuclableWithDelimiter.getCalculables());
    }

    @Test
    public void get_delimiter_calculated_from_string() {
        final var calcuclableWithDelimiter = new CalcuclableWithDelimiter("//xD\n2xD2");
        assertEquals(Pattern.quote("xD"), calcuclableWithDelimiter.getDelimiter());
        assertEquals("2xD2", calcuclableWithDelimiter.getCalculables());
    }

    @Test
    public void get_any_length_delimiter_calculated_from_string() {
        final var calcuclableWithDelimiter = new CalcuclableWithDelimiter("//*********\n2*********2");
        assertEquals(Pattern.quote("*********"), calcuclableWithDelimiter.getDelimiter());
        assertEquals("2*********2", calcuclableWithDelimiter.getCalculables());
    }

    @Test
    public void get_combined_delimiter() {
        final var calcuclableWithDelimiter = new CalcuclableWithDelimiter("//[#][&][%]\n2#2");
        final var expected = Pattern.quote("#")
                + "|" + Pattern.quote("&")
                + "|" + Pattern.quote("%");
        assertEquals(expected, calcuclableWithDelimiter.getDelimiter());
        assertEquals("2#2", calcuclableWithDelimiter.getCalculables());
    }
}